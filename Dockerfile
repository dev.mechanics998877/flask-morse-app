FROM alpine

RUN apk add --no-cache python3 py3-pip && pip3 install flask

WORKDIR /app

COPY ./flask-app .

EXPOSE 5000

ENTRYPOINT [ "python3", "flask_morse_app_01.py" ]
